// Tính tiền lương nhân viên
function tinhTienLuong(){
    var luong1Ngay = document.getElementById("luong-1-ngay").value*1;
    var soNgayLam = document.getElementById("so-ngay-lam").value*1;
    var tienLuong = luong1Ngay*soNgayLam;
    document.getElementById("result").innerHTML = `${tienLuong}`;
}
// Tính giá trị trung bình
function tinhTrungBinh(){
    var soThuNhat = document.getElementById("so-thu-1").value*1;
    var soThuHai = document.getElementById("so-thu-2").value*1;
    var soThuBa = document.getElementById("so-thu-3").value*1;
    var soThuTu = document.getElementById("so-thu-4").value*1;
    var soThuNam = document.getElementById("so-thu-5").value*1;
    var gttb = (soThuNhat + soThuHai + soThuBa + soThuTu + soThuNam)/5;
    document.getElementById("result1").innerHTML = `${gttb}`;
}
// Quy đổi tiền
function quyDoiTien(){
    var USD = document.getElementById("USD").value*1;
    var VND = USD*23500;
    VND = VND.toLocaleString('vi-VN');
    document.getElementById("result2").innerHTML = `${VND}`;
}
// Tính diện tích chu vi, hình chữ nhật
function hinhChuNhat(){
    var chieuDai = document.getElementById("chieu-dai").value*1;
    var chieuRong = document.getElementById("chieu-rong").value*1; 
    var chuVi = (chieuDai+chieuRong)*2;
    var dienTich = chieuDai*chieuRong;
    document.getElementById("result3").innerHTML = `Diện tích: ${dienTich}; Chu vi: ${chuVi}`;
}
// Tính tổng hai kí số
function tongHaiKiSo(){
    var so = document.getElementById("so").value*1;
    var chuc = Math.floor(so/10);
    var donvi = so%10;
    var tong = chuc + donvi;
    document.getElementById("result4").innerHTML = `${tong}`;
}